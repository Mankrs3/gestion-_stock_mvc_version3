package com.stock.mvc.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.taglibs.standard.tag.common.core.ForEachSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stock.mvc.entites.Article;
import com.stock.mvc.entites.Client;
import com.stock.mvc.entites.CommandeClient;
import com.stock.mvc.entites.LigneCommandeClient;
import com.stock.mvc.export.FileExporter;
import com.stock.mvc.model.ModelCommandeClient;
import com.stock.mvc.model.StringResponse;
import com.stock.mvc.services.IArticleService;
import com.stock.mvc.services.IClientService;
import com.stock.mvc.services.ICommandeClientService;
import com.stock.mvc.services.ILigneCommandeClientService;

@Controller
@RequestMapping(value="/commandeclient")
public class CommandeClientController {
	
	@Autowired
	private ICommandeClientService commandeService;
	@Autowired
	ILigneCommandeClientService ligneCdeService;
	
	@Autowired
	private IClientService clientService;
	
	@Autowired
	private IArticleService articleService;
	
	@Autowired
	private ModelCommandeClient modelCommande;
   
	
	@RequestMapping(value="/")
	public String index(Model  model) {
		List<CommandeClient> commandesClient=commandeService.selectAll();
		if(commandesClient.isEmpty()) {
			commandesClient=new ArrayList<CommandeClient>();
			
		}
		else {
			for (CommandeClient commandeClient : commandesClient) {
				List<LigneCommandeClient> ligneCdeClt=ligneCdeService.getByIdCommande(commandeClient.getIdCommandeClient());
			commandeClient.setLigneCommandeClients(ligneCdeClt);
			
			}
			
		}
		List<CommandeClient> commandedate=commandeService.commandeParDate();
		if(commandedate.isEmpty()) {
		System.out.println("commande="+commandedate);
		}
	for (CommandeClient commandeClient : commandedate) {
		System.out.println(commandeClient.getDateCommande());
		
	}
		
		
		
		model.addAttribute("commandesClient",commandesClient);
		return "commandeclient/commandeclient";
		
	}
	@RequestMapping(value="/nouveau")
	public String nouvelleCommande(Model model) { 
		
		List<Client> clients=clientService.selectAll();
		if(clients.isEmpty()) {
			clients=new ArrayList<Client>();
		}
		modelCommande.creerCommande();
		model.addAttribute("code",modelCommande.getCommande().getCode());
		model.addAttribute("dateCde",modelCommande.getCommande().getDateCommande());
		model.addAttribute("clients",clients);
		
		return "commandeclient/nouvellecommande";
	}
	
		
		@RequestMapping(value ="/creerCommande")
		@ResponseBody	
	public String	creerCommande(final Long idClient) {
			
			System.out.println(idClient);
		if(idClient==null) {
			return null;
		}
		Client client=clientService.getById(idClient);
		System.out.println(client.getNom());
	if(client==null) {
		
		return null;
		
	}else {
		modelCommande.modifierCommande(client);
		System.out.println("operation termines");
		
	}
		
		return "home/home";
		
		
		
		}
		
		
		
		@RequestMapping(value ="/ajouterLigne")
		@ResponseBody
		public  LigneCommandeClient getArticleByCode(final Long codeArticle) {
			System.out.println(codeArticle);
			if(codeArticle == null) {
				return null;
				
			}
			Article article=articleService.findOne("codeArticle",""+codeArticle);
			System.out.println(article); 
			if(article == null) {
				System.out.println("rien");
				return null;
				
			}
			//System.out.println(article);
			LigneCommandeClient ligne=modelCommande.ajouterLigneCommande(article);
			return ligne;
			
		}

		
		@RequestMapping(value ="/supprimerLigne")
		@ResponseBody
		public LigneCommandeClient supprimerLigneCommande(final Long idArticle) {
			if(idArticle==null) {
				return null;
			}
			Article article=articleService.getById(idArticle);
			if(article==null) {
				return null;
			}
			System.out.println("supprimer ligne ok");
			return modelCommande.supprimerLigneCommande(article);
		}
		@RequestMapping(value ="/enrigstrerCommande")
		@ResponseBody
	public String enrgistrerCommande() {
			CommandeClient nouvelleCommande=null;
			if(modelCommande.getCommande() !=null) {
				nouvelleCommande=commandeService.save(modelCommande.getCommande());
			}
			if(nouvelleCommande !=null) {
			Collection<LigneCommandeClient> ligneCommandes=modelCommande.getLignesCommandeClient(nouvelleCommande);
			if(ligneCommandes !=null && !ligneCommandes.isEmpty()) {
				
				for(LigneCommandeClient ligneCommandeClient : ligneCommandes) {
					ligneCdeService.save(ligneCommandeClient);
				} 
				modelCommande.init();
			}
			
			}
			
			System.out.println("--------------operartion redirect------------------");
		return "commandeclient/commandeclient";
	}
		@RequestMapping(value="/supprimer/{idcommandeclient}")
	public String supprimercommande(@PathVariable Long idcommandeclient) {
			System.out.println("id="+idcommandeclient);
			
			if(idcommandeclient==null) {
				return null;
			}
			commandeService.remove(idcommandeclient);
		
		return "redirect:/commandeclient/";
	}
		
		
		
		
		
		
}
