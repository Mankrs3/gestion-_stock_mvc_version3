package com.stock.mvc.controllers;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.stock.mvc.entites.Category;

import com.stock.mvc.services.ICategoryService;


@Controller
@RequestMapping(value = "/category")

public class CategoryController {
	
	@Autowired
	private ICategoryService categoryService;
	

	@RequestMapping(value = "/")
	public String category(Model model) {
		List<Category> categorys = categoryService.selectAll();
		 for(Category category:categorys) {
	        	System.out.println(category.getCode());
	        }

		if (categorys == null) {
			categorys = new ArrayList<Category>();

		}
		model.addAttribute("categorys", categorys);
		return "categorie/category";
	}
	
	@RequestMapping(value = "/nouveau", method = RequestMethod.GET)

	public String ajouterCategory(Model model) {
		Category category = new Category();
		//category.setNom("ali");
		model.addAttribute("category", category);
		return "categorie/ajouterCategory";
	}
	
	@RequestMapping(value = "/enregistrer", method = RequestMethod.POST)
	public String enregistrerCategory(Model model,Category category, MultipartFile file) {
			
		if (category != null) {
			
			if(category.getIdCategory()!=null) {
			categoryService.update(category);
		}else {
		categoryService.save(category);
		}
		
			
		}
		
		
		return "redirect:/category/";
	}
	@RequestMapping(value="/modifier/{idCategory}")
	public String modifierCategory(Model model,@PathVariable Long idCategory) {
		if(idCategory !=null) {
			Category category=categoryService.getById(idCategory);
			System.out.println(category);
			if(category !=null) {
				model.addAttribute("category", category);
			}
		}
		
		return "categorie/ajouterCategory";
	}
	@RequestMapping(value="/supprimer/{idCategory}")
	public String supprimerCategory(Model model,@PathVariable Long idCategory) {
		if(idCategory!=null) {
			Category category=categoryService.getById(idCategory);
			if(category!=null) {
				categoryService.remove(idCategory);
			}
		}
	  
		
		
		return"redirect:/category/";
	}
	

}
