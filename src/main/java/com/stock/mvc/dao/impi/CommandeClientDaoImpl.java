package com.stock.mvc.dao.impi;



import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.hibernate.type.CalendarDateType;

import com.stock.mvc.dao.ICommandeClientDao;
import com.stock.mvc.entites.CommandeClient;

public class CommandeClientDaoImpl extends GenericDaoImpl<CommandeClient> implements  ICommandeClientDao{

	@Override
	public List<CommandeClient> commandeParDate() {
		Date commande=null;
		String DT="2019-06-07";
		SimpleDateFormat sd=new SimpleDateFormat("yyyy-dd-mm");
		try {
		 commande=sd.parse(DT);
		 System.out.println(commande);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Query query=em.createQuery("select u from CommandeClient u where u.dateCommande=:x");
		query.setParameter("x",commande,TemporalType . DATE );
		
		return query.getResultList();
	}


	
	

}
