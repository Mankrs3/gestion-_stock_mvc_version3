package com.stock.mvc.dao;

import java.util.List;

import com.stock.mvc.entites.CommandeClient;

public interface ICommandeClientDao extends IGenericDao<CommandeClient> {
	

	List<CommandeClient> commandeParDate();

}
