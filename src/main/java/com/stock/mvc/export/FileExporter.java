package com.stock.mvc.export;


import javax.servlet.http.HttpServletResponse;

public interface FileExporter {
	
	boolean exporDataToExcel(HttpServletResponse response,String fileName,String encodage);
	boolean  importDataFormExcel();

}
