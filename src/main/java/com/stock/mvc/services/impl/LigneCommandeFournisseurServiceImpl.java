package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.ILigneCommandeFournisseurDao;
import com.stock.mvc.entites.LigneCommandeFournisseur;
import com.stock.mvc.services.ILigneCommandeFournisseurService;
 @Transactional 
public class LigneCommandeFournisseurServiceImpl implements ILigneCommandeFournisseurService{
	 private ILigneCommandeFournisseurDao dao;
	   

	public ILigneCommandeFournisseurDao getDao() {
		return dao;
	}

	public void setDao(ILigneCommandeFournisseurDao dao) {
		this.dao = dao;
	}

	public LigneCommandeFournisseur save(LigneCommandeFournisseur entity) {
	
		return  dao.save(entity) ;
	}

	
	public LigneCommandeFournisseur update(LigneCommandeFournisseur entity) {
		
		return dao.update(entity);
	}


	public List<LigneCommandeFournisseur> selectAll() {
		
		return dao.selectAll();
	}

	
	public List<LigneCommandeFournisseur> selectAll(String sortField, String sort) {
	
		return dao.selectAll(sortField, sort);
	}


	public LigneCommandeFournisseur getById(Long id) {
		 
		return dao.getById(id);
	}

	
	
	public void remove(Long id) {
		dao.remove(id);
		
		
	}


	public LigneCommandeFournisseur findOne(String paramName, Object paramValue) {
		
		return dao.findOne(paramName, paramValue);
	}

	
	public LigneCommandeFournisseur findOne(String[] paramNames, Object[] paramValues) {
	
		return dao.findOne(paramNames, paramValues);
	}

	
	public int findCountBy(String paramName, String paramValue) {
		
		return dao.findCountBy(paramName, paramValue);
	}

}
