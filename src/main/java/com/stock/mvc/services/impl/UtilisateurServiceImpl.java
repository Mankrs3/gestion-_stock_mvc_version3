package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.IUtilisateurDao;
import com.stock.mvc.entites.Utilisateur;
import com.stock.mvc.services.IUtilisateurService;
 @Transactional 
public class UtilisateurServiceImpl implements IUtilisateurService{
	 private IUtilisateurDao dao;
	   

	public IUtilisateurDao getDao() {
		return dao;
	}

	public void setDao(IUtilisateurDao dao) {
		this.dao = dao;
	}

	public Utilisateur save(Utilisateur entity) {
	
		return  dao.save(entity) ;
	}

	
	public Utilisateur update(Utilisateur entity) {
		
		return dao.update(entity);
	}


	public List<Utilisateur> selectAll() {
		
		return dao.selectAll();
	}

	
	public List<Utilisateur> selectAll(String sortField, String sort) {
	
		return dao.selectAll(sortField, sort);
	}


	public Utilisateur getById(Long id) {
		 
		return dao.getById(id);
	}

	
	
	public void remove(Long id) {
		dao.remove(id);
		
		
	}


	public Utilisateur findOne(String paramName, Object paramValue) {
		
		return dao.findOne(paramName, paramValue);
	}

	
	public Utilisateur findOne(String[] paramNames, Object[] paramValues) {
	
		return dao.findOne(paramNames, paramValues);
	}

	
	public int findCountBy(String paramName, String paramValue) {
		
		return dao.findCountBy(paramName, paramValue);
	}

}
