package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.IMvtStkDao;
import com.stock.mvc.entites.MvtStk;
import com.stock.mvc.services.IMvtStkService;
 @Transactional 
public class MvtStkServiceImpl implements IMvtStkService{
	 private IMvtStkDao dao;
	   

	public IMvtStkDao getDao() {
		return dao;
	}

	public void setDao(IMvtStkDao dao) {
		this.dao = dao;
	}

	public MvtStk save(MvtStk entity) {
	
		return  dao.save(entity) ;
	}

	
	public MvtStk update(MvtStk entity) {
		
		return dao.update(entity);
	}


	public List<MvtStk> selectAll() {
		
		return dao.selectAll();
	}

	
	public List<MvtStk> selectAll(String sortField, String sort) {
	
		return dao.selectAll(sortField, sort);
	}


	public MvtStk getById(Long id) {
		 
		return dao.getById(id);
	}

	
	
	public void remove(Long id) {
		dao.remove(id);
		
		
	}


	public MvtStk findOne(String paramName, Object paramValue) {
		
		return dao.findOne(paramName, paramValue);
	}

	
	public MvtStk findOne(String[] paramNames, Object[] paramValues) {
	
		return dao.findOne(paramNames, paramValues);
	}

	
	public int findCountBy(String paramName, String paramValue) {
		
		return dao.findCountBy(paramName, paramValue);
	}

}
